document.onreadystatechange = updateClock()

function updateClock() {
    const date = new Date()
    document.getElementById("current-time-text").innerText = date.toLocaleTimeString([], { hourCycle: 'h23' })
    const hour = date.getHours()

    if (hour < 5 || hour >= 18) {
        document.getElementById("time-greeting").innerText = "Good evening."
    }
    else if (hour < 12) {
        document.getElementById("time-greeting").innerText = "Good morning."
    }
    else {
        document.getElementById("time-greeting").innerText = "Good afternoon."
    }
}

setInterval(updateClock, 1000)

function redirectToPageWithPostData(url, data) {
    // Create a form element
    const form = document.createElement('form');
    form.method = 'POST';
    form.action = url;

    // Add hidden input elements to the form for each data key-value pair
    for (const key in data) {
        if (data.hasOwnProperty(key)) {
            const hiddenField = document.createElement('input');
            hiddenField.type = 'hidden';
            hiddenField.name = key;
            hiddenField.value = data[key];

            form.appendChild(hiddenField);
        }
    }

    // Append the form to the document, submit it, and then remove it
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function doSearch() {
    const query = document.getElementById('search-input').value
    let url = "";
    let data = { };

    switch (document.getElementById("search-engine-select").value) {
        case 'TJ\'s SearXNG Instance':
            url = 'https://searxng.icrs.cc/search';
            data = { q: `${query}` };
			break;
        case 'TJ\'s LibreX Instance':
            url = 'https://librex.icrs.cc/search.php';
            data = { q: `${query}` };
            break;
        case 'DuckDuckGo':
            url = 'https://duckduckgo.com/'
            data = { q: `${query}` };
            break;
		case 'Password Hash':
			url = './pw_hash.php';
			data = { password: `${query}` };
			break;
    }

    redirectToPageWithPostData(url, data);
}

const searchInput = document.getElementById("search-input")
searchInput.addEventListener("keypress", function onEvent(event) {
    if (event.key === "Enter") {
        document.getElementById("search-button").click();
    }
});
