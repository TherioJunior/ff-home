# ff-home

Homepage which I host on my server. Includes redirects to search <br>
with one of my two self-hosted search engines ([SearXNG](https://github.com/searxng/searxng) and [LibreX](https://github.com/hnhx/librex)) <br>
but also for DuckDuckGo if that's more your cup of tea.
